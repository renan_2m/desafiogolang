FROM golang:1.16.4-alpine AS builder

WORKDIR /go/src/app
COPY . .

RUN go build -ldflags "-s -w"

FROM scratch

WORKDIR /go/src/app
COPY --from=builder /go/src/app/golang /go/src/app/golang

ENTRYPOINT [ "./golang" ]